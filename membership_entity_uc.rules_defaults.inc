<?php

/**
 * @file
 * Default rules for membership_entity_uc.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function membership_entity_uc_default_rules_configuration() {
  $configs = array();
  $rule = '{ "rules_update_membership_status" : {
      "LABEL" : "Update membership status",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Membership" ],
      "REQUIRES" : [ "uc_order", "membership_entity_uc" ],
      "ON" : { "uc_order_status_update" : [] },
      "IF" : [
        { "uc_order_condition_has_product_class" : {
            "order" : [ "order" ],
            "product_classes" : { "value" : { "membership_entity_uc_product" : "membership_entity_uc_product" } },
            "required" : 0,
            "forbidden" : 0
          }
        }
      ],
      "DO" : [ { "membership_entity_uc_update_status" : { "order" : [ "order" ] } } ]
    }
  }';
  $configs['rules_update_membership_status'] = rules_import($rule);
  return $configs;
}
