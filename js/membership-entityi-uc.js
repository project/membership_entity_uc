/**
 * @file
 * JQuery functions to help with membership_entity_uc tasks.
 */

(function (window, document, $) {
  Drupal.behaviors.membership_entity_uc = {
    attach: function (context, settings) {
      membership_entity_uc_checkboxes();

      // If the product form checkbox is checked, make sure the join is too.
      $('#edit-instance-settings-membership-entity-product-form').click(function() {
        membership_entity_uc_checkboxes();
      });
    }
  };

  /**
   * Check to see the status of the product checkbox. Adjust accordingly.
   */
  function membership_entity_uc_checkboxes() {
    if ($('#edit-instance-settings-membership-entity-product-form').is(':checked')) {
      $('#edit-instance-settings-membership-entity-join-form').attr('checked', 'true');
      $('#edit-instance-settings-membership-entity-join-form').attr('disabled', 'true');
    }
    else {
      // Make sure required isn't checked.
      if (!$('#edit-instance-required').is(':checked')) {
        $('#edit-instance-settings-membership-entity-join-form').removeAttr('disabled');
      }
    }
  }
}(this, this.document, jQuery));
