<?php

/**
 * @file
 * Administrative tasks for the membership_entity_uc module.
 */

/**
 * Menu callback to build to product - term matching page.
 */
function membership_entity_uc_settings() {
  // Load all memberships.
  $memberships = membership_entity_get_bundles();
  $messages = array();
  $types = array();

  foreach ($memberships as $membership) {
    // Make sure terms have been added.
    if (!empty($membership->data)) {
      $types[$membership->type] = $membership->data['available_term_lengths'];
    }
    else {
      drupal_set_message(t('No terms set for membership type @membership. Click !terms to add terms.', array(
        '@membership' => $membership->type,
        '!terms' => l(t('here'), 'admin/memberships/types/manage/' . $membership->type),
      )));
    }
  }

  $messages = _membership_entity_uc_check_products($types);

  if (empty($messages)) {
    $messages[] = t('All terms have a matching product!');
  }

  // Loop through all terms and make sure a product exists.
  $term_helper = array(
    '#theme' => 'item_list',
    '#title' => t('The following terms have no associated products.'),
    '#items' => $messages,
  );

  return $term_helper;
}

/**
 * Helper function to return if a product exists for a given membership + term.
 *
 * @param array $data
 *   An array of memberships and terms.
 */
function _membership_entity_uc_check_products(array $data) {
  $messages = array();

  // Try to minimize the number of queries by using all terms at once.
  foreach ($data as $type => &$terms) {
    $results = db_select('membership_entity_product', 'mep')
      ->fields('mep')
      ->condition('type', $type)
      ->condition('term_length', $terms, 'IN')
      ->execute();

    // Loop through each item that exists and remove it from the message queue.
    foreach ($results as $result) {
      if (($key = array_search($result->term_length, $terms)) !== FALSE) {
        unset($terms[$key]);
      }
    }

    // Any remaining terms have not been created as products.
    foreach ($terms as $term) {
      $messages[] = t('A product has not been created for type :type and term :term', array(
        ':type' => $type,
        ':term' => $term,
      )) . ' - ' . l(t('create product'), 'node/add/' . strtr(MEMBERSHIP_ENTITY_UC_PCID, '_', '-'));
    }
  }
  // Clear memory.
  unset($terms);

  // Return all of the gathered messages.
  return $messages;
}
