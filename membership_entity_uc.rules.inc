<?php

/**
 * @file
 * Rules integration for the membership_entity_uc module.
 */

/**
 * Implements hook_rules_action_info().
 */
function membership_entity_uc_rules_action_info() {
  $actions['membership_entity_uc_update_status'] = array(
    'label' => t('Update a membership term status.'),
    'group' => t('Membership'),
    'base' => 'membership_entity_uc_update_status_from_order',
    'parameter' => array(
      'order' => array(
        'type' => 'uc_order',
        'label' => t('Order'),
        'description' => t('The order with membership products'),
        'save' => TRUE,
      ),
    ),
  );
  return $actions;
}

/**
 * Rules action callback.
 *
 * Update the status of a membership.
 */
function membership_entity_uc_update_status_from_order($order) {
  $state = uc_order_status_data($order->order_status, 'state');
  $statuses = membership_entity_get_status_list();
  switch ($state) {
    case 'canceled':
      $status = MEMBERSHIP_ENTITY_CANCELLED;
      break;

    case 'in_checkout':
    case 'post_checkout':
      $status = MEMBERSHIP_ENTITY_PENDING;
      break;

    case 'payment_received':
    case 'completed':
      $status = MEMBERSHIP_ENTITY_ACTIVE;
      break;

    default:
      return;
  }

  foreach ($order->products as $order_product) {
    $product = node_load($order_product->nid);
    if ($product->type != MEMBERSHIP_ENTITY_UC_PCID || empty($order_product->data['attributes']['Term ID'])) {
      continue;
    }

    $term = membership_entity_term_load(reset($order_product->data['attributes']['Term ID']));

    if ($term->status != $status) {
      $term->status = $status;
      membership_entity_term_save($term);
      watchdog('membership_entity_uc', 'Status updated to @status for membership @member_id', array(
        '@status' => $statuses[$status],
        '@member_id' => $term->mid,
      ), WATCHDOG_INFO, l(t('View membership'), 'membership/' . $term->mid));
    }
  }
}
