--------------------------------------------------------------------------------
                           Membership Entity UC
--------------------------------------------------------------------------------

Maintainer: Bryan Jones (Bartuc)

The Membership Entity UC module provides integration with Membership Entity by
overriding the Membership Entity register forms and redircting to the cart or
store admin pages (depending on role).

Project: http://drupal.org/project/membership_entity_uc

Installation
------------
Drush is the easiest way to install and enable the module.
  drush dl membership_entity_uc
  drush en membership_entity_uc

If you dont have Drush, do the following.
  Download the module from http://drupal.org/project/membership_entity_uc.
  Place the module in your site's module directory (usually sites/all/modules)
  Enable the module through the Drupal interface.

Configuration
-------------
The only task that needs to be done is to match products with terms. Once the
module is enabled you will have a new content type called Membership Product.

Create a product for each membership type and term you have and map them in
the Membership Product creation screen. Membership Entity UC will take care of
the rest.

How to Use
----------
Once the module is installed and configured it will do its magic in the
background.
